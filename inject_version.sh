#!/bin/bash

githash=$(git rev-parse --short HEAD)
num=$(date +%s)

hash_line="VUE_APP_COMMIT_HASH="
num_line="VUE_APP_COMMIT_NUM="

if [ ! -f .env ]
then
    touch .env
    echo "VUE_APP_COMMIT_HASH=99999" >> .env
    echo "VUE_APP_COMMIT_NUM=999999" >> .env
fi

sed -i "s|${hash_line}.*$|${hash_line}${githash}|g" .env
sed -i "s|${num_line}.*$|${num_line}${num}|g" .env
