export type HomeId = {
    wacnId: number;
    systemId: number;
    rfssId: number;
};

export type FleetManagerTokenType = {
    name: string;
    username: string;
    access_level: Record<string, number>;
    is_fleet_admin: number; // 0 or 1
    session_id: string;
    login_type: string;
    home_id: HomeId;
};

export type FieldType = {
    field: string;
    header: string;
    secondaryField?: string;
    dependentFields?: DependentFieldsType;
    defaultValue?: string | number;
    editable?: boolean;
    alwaysDisplay?: boolean;
    options?: Array<DropdownType>;
    selectable?: boolean;
    sortable?: boolean;
    size?: number;
    combo?: { key: string; location: number; };
    ipPools?: IpPoolsType[];
    type: "text" | "number" | "dropdown" | "binary" | "date" |
    "multiselect" | "datePeriods" | "hex" | "combo" | "ipPools" |
    "secondsAsInt" | "sgidLookup";
    agencyLookupFields?: { agencyFieldKey: string; sgidFieldKey: string; };
};

export type NavChild = {
    title: string;
    url: string;
};

export type NavParent = {
    title: string;
    baseUrl: string;
    children: NavChild[];
};

export type DropdownType = {
    value: number | string;
    name: string;
};

export type IpPoolsType = {
    poolId: string;
    static: boolean;
};

export type DependentFieldsType = {
    matchAll: boolean;
    dependsOn: DependsOnType[];
};

export type DependsOnType = {
    name: string;
    validValues: (boolean | string | number)[];
};

export type ChildRouteType = {
    title: string;
    url: string;
    requiredAccess?: number;
};

export type StatusBarItemType = {
    title: string;
    item_name: string;
};
