import { State } from "./index"

export default {
    getTitle(state: State): string {
        return state.title;
    },
    getSubtitle(state: State): string {
        return state.subtitle;
    },
}
