import { MUTATIONS, State } from "./index";
import { BASE_URL } from "@/utils/constants";

import type { ActionContext } from "vuex";
import axios from "axios";

export const enum ACTIONS {
  SET_HOSTNAME = "SET_HOSTNAME",
}

export default {
  async [ACTIONS.SET_HOSTNAME](ctx: ActionContext<State, unknown>): Promise<void> {
    try {
      const response = await axios.get(`${BASE_URL}/info/hostname`);
      ctx.commit(MUTATIONS.SET_HOSTNAME, response.data);
    } catch (error) {
      console.error(error);
    }
  }
};
