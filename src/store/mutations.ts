import { State } from "./index";

export const enum MUTATIONS {
  SET_HOSTNAME = "SET_HOSTNAME",
}

export default {
  [MUTATIONS.SET_HOSTNAME](state: State, hostname: string): void {
    state.subtitle = hostname;
  }
};
