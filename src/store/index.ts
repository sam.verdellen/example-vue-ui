import { InjectionKey } from 'vue';
import { createStore, useStore as baseStore, Store } from 'vuex';
import Getters from './getters';
import Mutations from './mutations';
import Actions from "./actions";

export interface State {
  title: string;
  subtitle: string;
}

export const key: InjectionKey<Store<State>> = Symbol();

export const store = createStore<State>({
  state: {
    title: "Example UI",
    subtitle: "", // Set the hostname via dispatch
  },
  getters: Getters,
  mutations: Mutations,
  actions: Actions,
});

export function useStore(): Store<State> {
  return baseStore(key);
}

export { ACTIONS } from './actions';
export { MUTATIONS } from './mutations';