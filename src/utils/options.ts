import type { DropdownType } from "@/types";

/**Provide the definition of commonly reused dropdown options */
export default class {
    // General
    static readonly priority: DropdownType[] = [
        { name: "1", value: 1 },
        { name: "2", value: 2 },
        { name: "3", value: 3 },
        { name: "4", value: 4 },
        { name: "5", value: 5 },
        { name: "6", value: 6 },
        { name: "7", value: 7 },
        { name: "8", value: 8 },
        { name: "9", value: 9 },
        { name: "10", value: 10 }
    ];

    static readonly yesNo: DropdownType[] = [
        { name: "yes", value: 1 },
        { name: "no", value: 0 }
    ];

    static readonly callPermission: DropdownType[] = [
        { name: "None", value: 0 },
        { name: "Non-emergency only", value: 1 },
        { name: "Emergency only", value: 2 },
        { name: "All", value: 3 }
    ];

    // Subscribers
    static readonly subscriberStatus: DropdownType[] = [
        { name: "Unregistered", value: 0 },
        { name: "Registered", value: 1 },
        { name: "Waiting for validation", value: 2 },
        { name: "Unknown subscriber", value: 3 },
        { name: "Blocked", value: 4 },
        { name: "No home RFSS", value: 5 }
    ];

    static readonly subscriberType: DropdownType[] = [
        { name: "Unit", value: 0 },
        { name: "RFSS", value: 1 },
        { name: "FNE", value: 2 }
    ];

    static readonly subscriberUnit: DropdownType[] = [
        { name: "SU", value: 0 },
        { name: "CSSI", value: 1 },
        { name: "TAG", value: 2 }
    ];


    // Groups
    static readonly groupType: DropdownType[] = [
        { name: "Standard", value: 0 },
        { name: "Announcement", value: 1 },
        { name: "System", value: 2 }
    ];

    static readonly groupStatus: DropdownType[] = [
        { name: "Inactive", value: 0 },
        { name: "Active", value: 1 },
        { name: "Waiting for validation", value: 2 },
        { name: "Blocked", value: 3 }
    ];

    static readonly groupMembership: DropdownType[] = [
        { name: "Open", value: 0 },
        { name: "Open with Restrictions", value: 1 },
        { name: "Closed", value: 2 }
    ];

    static readonly groupSharedAgencies: DropdownType[] = [
        { name: "No", value: "No" },
        { name: "Yes", value: "Yes" },
        { name: "All", value: "All" }
    ];

    // IP Pool
    static readonly nat: DropdownType[] = [
        { name: "IPv4 Static", value: "1" },
        { name: "IPv4 Dynamic", value: "0" }
    ];

    // Location
    static readonly location: DropdownType[] = [
        { name: "Unknown", value: 0 },
        { name: "RFSS", value: 1 },
        { name: "Console", value: 2 },
        { name: "PSTNG", value: 3 },
        { name: "RLR", value: 4 },
        { name: "Site", value: 5 },
        // { name: "Mobile IP HA", value: 6 } Left out as not in use
        { name: "Voice Recorder", value: 7 }
    ];

    static readonly vocoder: DropdownType[] = [
        { name: "Full Rate", value: 0 },
        { name: "Half Rate", value: 1 },
        { name: "Native", value: 2 },
        { name: "Unknown", value: 3 }
    ];

    // Super Groups
    static readonly regroupStatus: DropdownType[] = [
        { name: "Inactive", value: 0 },
        { name: "Active", value: 1 }
    ];
}
