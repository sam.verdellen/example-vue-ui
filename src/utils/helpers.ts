/**Generic replacer to use when converting json objects to a string*/
export function replacer(key: string, value: string | number | boolean): string | number {
    if (typeof value === "boolean") {
        return value ? 1 : 0;
    }
    return value;
}