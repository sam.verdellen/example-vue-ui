import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import { store, key } from './store';
import axios from "axios";

import PrimeVue from 'primevue/config';
import Button from 'primevue/button';
import InputText from 'primevue/inputtext';
import InputNumber from 'primevue/inputnumber';
import Checkbox from 'primevue/checkbox';
import { VueCookieNext } from 'vue-cookie-next';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup';
import Dropdown from 'primevue/dropdown';
import ToggleButton from 'primevue/togglebutton';
import Calendar from 'primevue/calendar';
import MultiSelect from 'primevue/multiselect';
import ToastService from 'primevue/toastservice';
import Toast from 'primevue/toast';
import Tooltip from 'primevue/tooltip';
import Dialog from 'primevue/dialog';
import ScrollPanel from 'primevue/scrollpanel';
import 'primevue/resources/themes/saga-blue/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';

import Layout from '@/components/Layout.vue';
import { i18n } from './lang/index';
import { getPrimevueLocaleMessages } from './lang/index';
import ConfirmationService from 'primevue/confirmationservice';
import ConfirmDialog from 'primevue/confirmdialog';
import ProgressSpinner from 'primevue/progressspinner';
import Accordion from 'primevue/accordion';
import AccordionTab from 'primevue/accordiontab';
import Panel from 'primevue/panel';


const app = createApp(App);

app.use(store, key);
app.use(router);

app.use(PrimeVue, getPrimevueLocaleMessages());
app.use(VueCookieNext);
app.use(i18n);
app.use(ToastService);
app.use(ConfirmationService);

app.component("primevue-inputtext", InputText);
app.component("primevue-progressspinner", ProgressSpinner);
app.component("primevue-confirmdialog", ConfirmDialog);
app.component("primevue-inputnumber", InputNumber);
app.component("primevue-checkbox", Checkbox);
app.component("primevue-button", Button);
app.component("primevue-toast", Toast);
app.component("primevue-datatable", DataTable);
app.component("primevue-column", Column);
app.component("primevue-column-group", ColumnGroup);
app.component("primevue-dropdown", Dropdown);
app.component("primevue-togglebutton", ToggleButton);
app.component("primevue-calendar", Calendar);
app.component("primevue-multiselect", MultiSelect);
app.component("primevue-dialog", Dialog);
app.component("primevue-accordion", Accordion);
app.component("primevue-accordion-tab", AccordionTab);
app.component("primevue-scrollpanel", ScrollPanel);
app.component("primevue-panel", Panel);
app.component("tait-layout", Layout);

app.directive("tooltip", Tooltip);

app.mount('#app');

// set default config
VueCookieNext.config({ expire: '7d' });

// This interceptor is suppose to inject
axios.interceptors.request.use(config => {
  const is_websocket_request = config.url?.includes("rlr/websocket/");
  // Do something before request is sent
  if (is_websocket_request) {
    config.headers.authorization = "some auth";
  }
  else {
    config.headers.authorization = "some auth";
  }
  console.log(config.url);
  return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});