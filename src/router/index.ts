import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: { name: "Login" }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login.vue')
  },
  {
    path: '/testtable',
    name: 'TestTable',
    components: {
      default: () => import('@/components/Layout.vue'),
      context: () => import('@/views/TestTable.vue')
    }
  },
  {
    path: '/testform',
    name: 'TestForm',
    components: {
      default: () => import('@/components/Layout.vue'),
      context: () => import('@/views/TestForm.vue')
    },
    props: {
      default: false,
      context: true
    }
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
