export default {
  Username: "Username",
  Password: "Password",
  LoginWarning1: "WARNING! THIS SYSTEM IS RESTRICTED TO AUTHORIZED USERS ONLY!",
  LoginWarning2: "If you are not authorized to use this system, you must exit immediately. Unauthorized users will be subject to criminal penalties, fines, damages and/or disciplinary action.",
  LoginWarning3: "If you are authorized to use this system, you must do so in compliance with all laws, regulations, conduct rules, and company security policies applicable to this system. This system, including any hardware components, software, work stations, and storage spaces, is subject to monitoring and search without advance notice. Users should have no expectation of privacy in their use of any aspect of this system.",
  LoginAgreeTerms: "By using this Tait software, you accept the terms of the ",
  TaitAgreement: "Tait General Software License Agreement",

  Login: "Login",
  ConfirmChanges: "Confirm Changes",
  Edit: "Edit",

  Services: "Services",
  About: "About",
  Help: "Help",
  Logout: 'Logout',

  FleetAdmin: "Fleet Administrator"
}
