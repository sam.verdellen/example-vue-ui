export default {
    Username: "Nome utente",
    Password: "Parola d'ordine",
    LoginWarning1: "AVVERTIMENTO! QUESTO SISTEMA È LIMITATO SOLO AGLI UTENTI AUTORIZZATI!",
    LoginWarning2: "Se non si è autorizzati a utilizzare questo sistema, è necessario uscire immediatamente. Gli utenti non autorizzati saranno soggetti a sanzioni penali, multe, danni e/o azioni disciplinari.",
    LoginWarning3: "Se sei autorizzato a utilizzare questo sistema, devi farlo in conformità con tutte le leggi, i regolamenti, le regole di condotta e le politiche di sicurezza aziendali applicabili a questo sistema. Questo sistema, inclusi componenti hardware, software, stazioni di lavoro e spazi di archiviazione, è soggetto a monitoraggio e ricerca senza preavviso. Gli utenti non dovrebbero aspettarsi privacy nel loro uso di qualsiasi aspetto di questo sistema.",
    LoginAgreeTerms: "Utilizzando questo software Tait, accetti i termini del ",
    TaitAgreement: "Contratto di licenza software generale Tait",

    Login: "Accesso",
    ConfirmChanges: "Conferma modifiche",
    Edit: "Redigere",

    Services: "Servizi",
    About: "Circa",
    Help: "Guida",
    Logout: 'Logout',

    FleetAdmin: "Amministratore Della Flotta"
  }