import { createI18n } from 'vue-i18n'
// User defined lang
import enLocale from './en'
import itLocale from './it'
import datetimeFormats from './datetimes'
import primevue_locales from './primevue_locales'


const messages = {
  en: {
    ...enLocale
  },
  it: {
    ...itLocale
  }
}

const getLocale = () => {
  // read language from cookie 
  const cookieLanguage = sessionStorage.getItem('language')

  if (cookieLanguage) {
    return cookieLanguage
  }
  // if none, get browser supported lang
  const language = navigator.language.toLowerCase()
  // get all supported langs
  const locales = Object.keys(messages)

  for (const locale of locales) {
    // if our supported messages has the language.
    if (language.indexOf(locale) > -1) {
      return locale
    }
  }
  // defaults to en
  return 'en'
}

export const getPrimevueLocaleMessages = (): string => {
  const cookieLanguage = sessionStorage.getItem('language')

  if (cookieLanguage) {
    return cookieLanguage
  }
  // if none, get browser supported lang
  const language = navigator.language.toLowerCase()
  // get all supported langs
  const locales = Object.keys(primevue_locales)

  for (const locale of locales) {
    // if our supported messages has the language.
    if (language.indexOf(locale) > -1) {
      return locale
    }
  }

  // defaults to en
  return 'en'
}

export const i18n = createI18n({
  locale: getLocale(),
  fallbackLocale: 'en',
  messages:messages,
  datetimeFormats:datetimeFormats
})